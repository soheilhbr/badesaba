const { ServiceBroker } = require("moleculer");
const mongoose = require('mongoose');
const studentsInfo=require('./student/actStudents')
mongoose.connect(`mongodb://localhost/badeSabaTestDB`,{autoIndex:true});
const broker = new ServiceBroker({
    nodeID: "actionStudent",
    transporter: "nats://localhost:4222",
    logLevel: "debug",
    requestTimeout: 5 * 1000
});
broker.createService(studentsInfo)
broker.start()   

// .catch(err => console.error(`Error occured! ${err.message}`));
