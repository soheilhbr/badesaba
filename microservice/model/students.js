const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StudentsInfoSchema = Schema({
    id : { type : String , required : true,unique:true },//شماره فرم
    dateComplete : { type : String , required : true },//تاریخ تکمیل فرم
    name: { type : String , required : true },
    lastName:{ type : String  },
    degree:{ type : String , required : true },
    point:{ type : String , required : true }
});

module.exports = mongoose.model('Students' , StudentsInfoSchema);