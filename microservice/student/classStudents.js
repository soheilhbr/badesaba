const student=require('../model/students');
const jalaali=require('jalaali-js');
class studentsInfo{
    constructor(){
      
    }
    async insertStudent(data){
        let miladiDate=new Date()
        let toJlli=jalaali.toJalaali(miladiDate);
        let jy=toJlli.jy<10?"0"+toJlli.jy.toString():toJlli.jy.toString();
        let jm=toJlli.jm<10?"0"+toJlli.jm.toString():toJlli.jm.toString();
        let jd=toJlli.jd<10?"0"+toJlli.jd.toString():toJlli.jd.toString();
      
        toJlli=jy+"/"+jm+"/"+jd+" "+`${miladiDate.getHours().toString()}:${miladiDate.getMinutes().toString()}`;
        let StudentObj={
            id : data.params.id,//شماره فرم
            dateComplete :toJlli ,//تاریخ تکمیل فرم
            name: data.params.name,
            lastName:data.params.lastName,
            degree:data.params.degree,
            point:data.params.point
        }  
        let studentNew=new student(StudentObj);
        let result=0;
        await studentNew.save().then((err)=>{             
            
            result=1;
        }).catch((err)=>{result=0;})
        return result;    
    }

    async editStudent(data){
        let result=0;
        await student.findOneAndUpdate({id:data.params.id},{
            name: data.params.name,
            lastName:data.params.lastName,
            degree:data.params.degree,
            point:data.params.point
          }).then((err,user)=>{
            
            result=1;
        }).catch((err)=>{result=0;})
        return result;
    }
    async removeStudent(data){
        let result=0;
       await student.findOneAndRemove({id:data.params.id}).then((res)=>{
            
            result=1;
        }).catch((err)=>{result=0;});
        return result;
    }
    async displayAll(){
        let result=null;
           await student.find({}).then((res)=>result=res);
        return result;
    }
}
module.exports=studentsInfo