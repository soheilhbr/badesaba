classStudents=require('./classStudents');
let studentsObj=new classStudents();
module.exports={
    name:'actionStudent',
    actions:{
        insertStudent:studentsObj.insertStudent,
        editStudent:studentsObj.editStudent,
        removeStudent:studentsObj.removeStudent,
        displayAll:studentsObj.displayAll,
    } 
}